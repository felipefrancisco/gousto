<?php declare(strict_types=1);

namespace Domain\Exceptions;

/**
 * Class InvalidRateRangeException
 * @package Domain\Exceptions
 */
class InvalidRateRangeException extends \Exception
{
    protected $code = 5;

    public static function withRate(int $rate): self
    {
        return new self(
            sprintf('Invalid Rate `%d`. Expected between 1 and 5.', $rate)
        );
    }
}
