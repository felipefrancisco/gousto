<?php declare(strict_types=1);

namespace Domain;

use MabeEnum\Enum;

/**
 * Class BoxType
 * @package Domain
 */
class BoxType extends Enum
{
    public const VEGETARIAN = 'vegetarian';
    public const GOURMET = 'gourmet';
}
