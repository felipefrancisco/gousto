<?php

namespace Tests\Functional;

use Domain\Recipe;
use Domain\Repositories\RecipeRepositoryInterface;
use Illuminate\Http\Request;
use Infrastructure\Http\Requests\RateRecipeByIdRequest;
use Infrastructure\Http\Requests\UpdateRecipeByIdRequest;
use Infrastructure\Repositories\Csv\CsvRecipeRepository;
use Test\TestCase;

class UpdateRecipeByIdTest extends TestCase
{
    /** @var RecipeRepositoryInterface */
    protected $recipeRepository;

    public function setUp()
    {
        parent::setUp();

        $this->recipeRepository = app(CsvRecipeRepository::class);
    }

    /** @test */
    public function itSucceedsToUpdateExistingRecipe(): void
    {
        $data = $this->validUpdateRecipePayload();

        $this->json('PUT', '/recipes/1', $data);

        $this->seeStatusCode(200);

        $recipe = $this->recipeRepository->findById(1);

        $this->assertRecipe($recipe);
    }

    /** @test */
    public function itFailsToUpdateNonExistingRecipe(): void
    {
        $data = $this->validUpdateRecipePayload();

        $this->json('PUT', '/recipes/2', $data);

        $this->seeStatusCode(404);
    }

    private function validUpdateRecipePayload(): array
    {
        return [
            "box_type" => "vegetarian",
            "title" => "Marmite",
            "slug" => "marmite",
            "short_title" => "",
            "marketing_description" => "Marmite",
            "calories_kcal" => 9999,
            "protein_grams" => 9999,
            "fat_grams" => 9999,
            "carbs_grams" => 999,
            "bulletpoint1" => "Bullet 1",
            "bulletpoint2" => "Bullet 2",
            "bulletpoint3" => "Bullet 3",
            "recipe_diet_type_id" => "fish",
            "season" => "season",
            "base" => "base",
            "protein_source" => "beef",
            "preparation_time_minutes" => 9999,
            "shelf_life_days" => 9999,
            "equipment_needed" => "equipment",
            "origin_country" => "Italy",
            "recipe_cuisine" => "asian",
            "in_your_box" => "one, two, three things",
            "gousto_reference" => 9999
        ];
    }

    private function assertRecipe(Recipe $recipe): void
    {
        $this->assertEquals("vegetarian", $recipe->boxType()->getValue());
        $this->assertEquals("Marmite", $recipe->title());
        $this->assertEquals("marmite", $recipe->slug());
        $this->assertEquals("", $recipe->shortTitle());
        $this->assertEquals("Marmite", $recipe->marketingDescription());
        $this->assertEquals(9999, $recipe->calories()->asKcal());
        $this->assertEquals(9999, $recipe->protein()->asGrams());
        $this->assertEquals(9999, $recipe->fat()->asGrams());
        $this->assertEquals(999, $recipe->carbs()->asGrams());
        $this->assertEquals("Bullet 1", $recipe->bulletpoints()->first());
        $this->assertEquals("Bullet 2", $recipe->bulletpoints()->second());
        $this->assertEquals("Bullet 3", $recipe->bulletpoints()->third());
        $this->assertEquals("fish", $recipe->dietType()->getValue());
        $this->assertEquals("season", $recipe->season());
        $this->assertEquals("base", $recipe->base());
        $this->assertEquals("beef", $recipe->protein()->source()->getValue());
        $this->assertEquals(9999, $recipe->preparationTime()->inMinutes());
        $this->assertEquals(9999, $recipe->shelfLife()->inDays());
        $this->assertEquals("equipment", $recipe->equipmentNeeded());
        $this->assertEquals("Italy", $recipe->country()->name());
        $this->assertEquals("asian", $recipe->cuisine()->getValue());
        $this->assertEquals("one, two, three things", $recipe->inYourBox()->toString());
        $this->assertEquals(9999, $recipe->goustoReference());
    }
}
