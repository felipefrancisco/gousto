<?php

namespace Infrastructure\Http\Transformers;

use Domain\Recipe;
use Domain\Repositories\RecipeRepositoryInterface;
use Illuminate\Support\Collection;
use Infrastructure\Repositories\Csv\CsvRecipeRepository;
use League\Fractal\Pagination\PaginatorInterface;

class RecipePaginator implements PaginatorInterface
{
    protected $csvRecipeRepository;

    protected $current;

    protected $limit;

    protected $items;

    public function __construct(
        CsvRecipeRepository $csvRecipeRepository,
        Collection $items,
        int $current,
        int $limit
    ) {
        $this->csvRecipeRepository = $csvRecipeRepository;
        $this->items = $items;
        $this->current = $current;
        $this->limit = $limit;
    }

    /**
     * Get the current page.
     *
     * @return int
     */
    public function getCurrentPage()
    {
        return $this->current;
    }

    /**
     * Get the last page.
     *
     * @return int
     */
    public function getLastPage()
    {
        return ceil($this->csvRecipeRepository->records()->count() / $this->limit);
    }

    /**
     * Get the total.
     *
     * @return int
     */
    public function getTotal()
    {
        $this->csvRecipeRepository->records()->count();
    }

    /**
     * Get the count.
     *
     * @return int
     */
    public function getCount()
    {
        // TODO: Implement getCount() method.
    }

    /**
     * Get the number per page.
     *
     * @return int
     */
    public function getPerPage()
    {
        // TODO: Implement getPerPage() method.
    }

    /**
     * Get the url for the given page.
     *
     * @param int $page
     *
     * @return string
     */
    public function getUrl($page)
    {
        // TODO: Implement getUrl() method.
    }
}
