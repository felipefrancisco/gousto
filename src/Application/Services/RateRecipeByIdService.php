<?php

namespace Application\Services;

use Application\UseCases\RateRecipeByIdUseCase;
use Domain\Repositories\RecipeRepositoryInterface;
use Infrastructure\Repositories\Strategies\SelfRatingStrategy;


class RateRecipeByIdService
{
    /** @var RecipeRepositoryInterface  */
    protected $recipeRepository;

    public function __construct(
        RecipeRepositoryInterface $recipeRepository
    ) {
        $this->recipeRepository = $recipeRepository;
    }

    public function handle(RateRecipeByIdUseCase $rateRecipeByIdUseCase): void
    {
        $recipe = $this->recipeRepository->findById(
            $rateRecipeByIdUseCase->getId()
        );

        $recipe->rate(
            $rateRecipeByIdUseCase->getRating()
        );

        $this->recipeRepository->save($recipe);
    }
}
