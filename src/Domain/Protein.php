<?php declare(strict_types=1);

namespace Domain;

/**
 * Class Protein
 * @package Domain
 */
class Protein
{
    protected $amount;
    protected $source;

    /**
     * Protein constructor.
     * @param int $amount
     * @param ProteinSource $source
     */
    public function __construct(int $amount, ProteinSource $source)
    {
        $this->amount = $amount;
        $this->source = $source;
    }

    /**
     * @return ProteinSource
     */
    public function source(): ProteinSource
    {
        return $this->source;
    }

    /**
     * @return int
     */
    public function amount(): int
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function asGrams(): int
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function asKilos(): int
    {
        return ($this->amount / 1000);
    }
}
