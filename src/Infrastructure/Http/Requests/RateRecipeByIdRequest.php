<?php declare(strict_types=1);

namespace Infrastructure\Http\Requests;

use Illuminate\Http\Request;

class RateRecipeByIdRequest extends Request
{
    public function rules()
    {
        return [
            'id' => 'required|integer',
            'rating' => 'required|integer'
        ];
    }

    public function getId(): int
    {
        $id = collect($this->all())->get('id');

        return (int)($id);
    }

    public function getRating(): int
    {
        return (int)($this->get('rating'));
    }
}
