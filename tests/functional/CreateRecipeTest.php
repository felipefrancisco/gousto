<?php

namespace Tests\Functional;

use Domain\Repositories\RecipeRepositoryInterface;
use Illuminate\Http\Request;
use Infrastructure\Http\Requests\CreateRecipeRequest;
use Infrastructure\Http\Requests\RateRecipeByIdRequest;
use Infrastructure\Http\Requests\UpdateRecipeByIdRequest;
use Infrastructure\Repositories\Csv\CsvRecipeRepository;
use Test\TestCase;

class CreateRecipeTest extends TestCase
{
    /** @var RecipeRepositoryInterface */
    protected $recipeRepository;

    public function setUp()
    {
        parent::setUp();

        $this->recipeRepository = app(CsvRecipeRepository::class);
    }

    /** @test */
    public function itSucceedsToCreateRecipe(): void
    {
        $data = $this->validCreateRecipePayload();

        $response = $this->json('POST', '/recipes', $data)->response->getContent();

        $this->seeStatusCode(201);

        $response = json_decode($response);
        $id = $response->data->id;

        // Assert it was properly updated
        $recipe = $this->recipeRepository->findById($id);

        $this->assertEquals(9999, $recipe->goustoReference());
        $this->assertEquals('Marmite', $recipe->title());
        $this->assertEquals('marmite', $recipe->slug());

        // Assert others haven't been affected
        $recipe = $this->recipeRepository->findById(1);

        $this->assertEquals(59, $recipe->goustoReference());
    }

    /** @test */
    public function itFailsWhenBoxTypeIsInvalid(): void
    {
        $data = $this->validCreateRecipePayload();
        $data['box_type'] = 'invalid';

        $this->json('POST', '/recipes', $data)->response;

        $this->seeStatusCode(400);
    }

    /** @test */
    public function itFailsWhenCuisineIsInvalid(): void
    {
        $data = $this->validCreateRecipePayload();
        $data['recipe_cuisine'] = 'invalid';

        $this->json('POST', '/recipes', $data);

        $this->seeStatusCode(400);
    }

    /** @test */
    public function itFailsWhenRequestIsMissingRequiredProperties(): void
    {
        $data = $this->validCreateRecipePayload();
        unset($data['recipe_cuisine']);

        $this->json('POST', '/recipes', $data);

        $this->seeStatusCode(422);
    }

    private function validCreateRecipePayload(): array
    {
        return [
            "box_type" => "vegetarian",
            "title" => "Marmite",
            "slug" => "marmite",
            "short_title" => "",
            "marketing_description" => "Marmite",
            "calories_kcal" => 9999,
            "protein_grams" => 9999,
            "fat_grams" => 9999,
            "carbs_grams" => 999,
            "bulletpoint1" => "Bullet 1",
            "bulletpoint2" => "Bullet 2",
            "bulletpoint3" => "Bullet 3",
            "recipe_diet_type_id" => "fish",
            "season" => "season",
            "base" => "base",
            "protein_source" => "beef",
            "preparation_time_minutes" => 9999,
            "shelf_life_days" => 9999,
            "equipment_needed" => "equipment",
            "origin_country" => "Italy",
            "recipe_cuisine" => "asian",
            "in_your_box" => "one, two, three things",
            "gousto_reference" => 9999
        ];
    }
}
