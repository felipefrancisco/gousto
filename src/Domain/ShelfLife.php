<?php declare(strict_types=1);

namespace Domain;

/**
 * Class ShelfLife
 * @package Domain
 */
class ShelfLife
{
    protected $days;

    /**
     * ShelfLife constructor.
     * @param int $days
     */
    public function __construct(int $days)
    {
        $this->days = $days;
    }

    /**
     * @return int
     */
    public function inDays(): int
    {
        return $this->days;
    }
}
