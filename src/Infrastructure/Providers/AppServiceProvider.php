<?php declare(strict_types=1);

namespace Infrastructure\Providers;

use Application\Services\CreateRecipeService;
use Application\Services\FetchAllRecipesByCuisineService;
use Application\Services\FetchRecipeByIdService;
use Application\Services\FetchRecipeByIdUseCase;
use Application\Services\RateRecipeByIdService;
use Application\Services\UpdateRecipeByIdService;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Factories\RecipeFactory;
use Infrastructure\Repositories\Csv\CsvRecipeRepository;

/**
 * Class AppServiceProvider
 * @package Infrastructure\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(FetchRecipeByIdService::class, function() {

            $csvRecipeRepository = app(CsvRecipeRepository::class);

            return new FetchRecipeByIdService(
                $csvRecipeRepository
            );
        });

        $this->app->singleton(RateRecipeByIdService::class, function() {

            $csvRecipeRepository = app(CsvRecipeRepository::class);

            return new RateRecipeByIdService(
                $csvRecipeRepository
            );
        });
        
        $this->app->singleton(UpdateRecipeByIdService::class, function() {

            $csvRecipeRepository = app(CsvRecipeRepository::class);

            return new UpdateRecipeByIdService(
                $csvRecipeRepository
            );
        });

        $this->app->singleton(CreateRecipeService::class, function() {

            $csvRecipeRepository = app(CsvRecipeRepository::class);
            $recipeFactory = app(RecipeFactory::class);

            return new CreateRecipeService(
                $csvRecipeRepository,
                $recipeFactory
            );
        });

        $this->app->singleton(FetchAllRecipesByCuisineService::class, function() {

            $csvRecipeRepository = app(CsvRecipeRepository::class);

            return new FetchAllRecipesByCuisineService(
                $csvRecipeRepository
            );
        });
    }
}
