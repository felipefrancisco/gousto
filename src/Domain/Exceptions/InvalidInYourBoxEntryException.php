<?php declare(strict_types=1);

namespace Domain\Exceptions;

/**
 * Class InvalidInYourBoxEntryException
 * @package Domain\Exceptions
 */
class InvalidInYourBoxEntryException extends \Exception
{
    protected $code = 3;

    /**
     * @param string $entry
     * @return InvalidInYourBoxEntryException
     */
    public static function withEntry(string $entry): self
    {
        return new self(
            sprintf('Invalid `InYourBox` entry `%s`. Expected `string`.', $entry)
        );
    }
}
