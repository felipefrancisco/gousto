<?php

namespace Application\Services;

use Domain\Repositories\RecipeRepositoryInterface;
use Application\UseCases\FetchAllRecipesByCuisineUseCase;
use Illuminate\Support\Collection;

class FetchAllRecipesByCuisineService
{
    protected $recipeRepository;

    public function __construct(
        RecipeRepositoryInterface $recipeRepository
    ) {
        $this->recipeRepository = $recipeRepository;
    }

    public function handle(FetchAllRecipesByCuisineUseCase $fetchAllRecipesByCuisineUseCase): Collection
    {
        $recipes = $this->recipeRepository->findByCuisine(
            $fetchAllRecipesByCuisineUseCase->getCuisine()
        );

        return $recipes;
    }
}
