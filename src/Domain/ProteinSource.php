<?php declare(strict_types=1);

namespace Domain;

use MabeEnum\Enum;

/**
 * Class ProteinSource
 * @package Domain
 */
class ProteinSource extends Enum
{
    public const BEEF = 'beef';
    public const SEAFOOD = 'seafood';
    public const PORK = 'pork';
    public const CHEESE = 'cheese';
    public const CHICKEN = 'chicken';
    public const EGGS = 'eggs';
    public const FISH = 'fish';
}
