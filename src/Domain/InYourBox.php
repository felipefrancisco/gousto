<?php declare(strict_types=1);

namespace Domain;

use Domain\Exceptions\InvalidInYourBoxEntryException;
use Illuminate\Support\Collection;

/**
 * Class InYourBox
 * @package Domain
 */
class InYourBox extends Collection
{
    /**
     * InYourBox constructor.
     * @param array $items
     * @throws \Exception
     */
    /**
     * InYourBox constructor.
     * @param array $items
     * @throws InvalidInYourBoxEntryException
     */
    public function __construct($items = [])
    {
        if (!is_array($items)) {
            parent::__construct([]);
            return;
        }

        foreach ($items as $item) {
            if (!is_string($item)) {
              throw InvalidInYourBoxEntryException::withEntry((string)($item));
            }
        }

        parent::__construct($items);
    }

    /**
     * @return string
     */
    /**
     * @return string
     */
    public function toString(): string
    {
        return $this->implode(', ');
    }
}
