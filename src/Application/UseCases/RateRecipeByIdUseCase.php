<?php

namespace Application\UseCases;

use Infrastructure\Http\Requests\RateRecipeByIdRequest;

class RateRecipeByIdUseCase
{
    /** @var int  */
    protected $id;

    /** @var int  */
    protected $rating;

    /**
     * RateRecipeByIdUseCase constructor.
     * @param int $id
     * @param int $rating
     */
    public function __construct(int $id, int $rating)
    {
        $this->id = $id;
        $this->rating = $rating;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }

    /**
     * @param RateRecipeByIdRequest $request
     * @return RateRecipeByIdUseCase
     */
    public static function fromRequest(RateRecipeByIdRequest $request): RateRecipeByIdUseCase
    {
        return new self(
            $request->getId(),
            $request->getRating()
        );
    }
}
