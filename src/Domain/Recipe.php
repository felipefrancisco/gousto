<?php declare(strict_types=1);

namespace Domain;

use Carbon\Carbon;
use Domain\Behaviours\RecipeSerialization;

/**
 * Class Recipe
 * @package Domain
 */
class Recipe implements \JsonSerializable
{
    use RecipeSerialization;

    /** @var int */
    protected $id;

    /** @var BoxType */
    protected $boxType;

    /** @var string */
    protected $title;

    /** @var string */
    protected $slug;

    /** @var string */
    protected $shortTitle;

    /** @var string */
    protected $marketingDescription;

    /** @var Calories */
    protected $calories;

    /** @var Carbs */
    protected $carbs;

    /** @var Fat */
    protected $fat;

    /** @var Protein */
    protected $protein;

    /** @var DietType */
    protected $dietType;

    /** @var string */
    protected $season;

    /** @var string */
    protected $base;

    /** @var PreparationTime */
    protected $preparationTime;

    /** @var BulletpointCollection */
    protected $bulletpoints;

    /** @var ShelfLife */
    protected $shelfLife;

    /** @var Cuisine */
    protected $cuisine;

    /** @var string */
    protected $equipmentNeeded;

    /** @var Country */
    protected $country;

    /** @var InYourBox */
    protected $inYourBox;

    /** @var int */
    protected $goustoReference;

    /** @var Rating  */
    protected $rating;

    /** @var Carbon */
    protected $createdAt;

    /** @var Carbon|null int */
    protected $updatedAt;

    public function __construct(
        ?int $id,
        BoxType $boxType,
        string $title,
        string $slug,
        ?string $shortTitle,
        string $marketingDescription,
        Calories $calories,
        Carbs $carbs,
        Fat $fat,
        Protein $protein,
        DietType $dietType,
        string $season,
        ?string $base,
        PreparationTime $preparationTime,
        BulletpointCollection $bulletpoints,
        ShelfLife $shelfLife,
        Cuisine $cuisine,
        string $equipmentNeeded,
        Country $country,
        InYourBox $inYourBox,
        int $goustoReference,
        ?Rating $rating,
        Carbon $createdAt,
        ?Carbon $updatedAt = null
    ) {
        $this->id = $id;
        $this->boxType = $boxType;
        $this->title = $title;
        $this->slug = $slug;
        $this->shortTitle = $shortTitle;
        $this->marketingDescription = $marketingDescription;
        $this->calories = $calories;
        $this->carbs = $carbs;
        $this->fat = $fat;
        $this->protein = $protein;
        $this->dietType = $dietType;
        $this->season = $season;
        $this->base = $base;
        $this->preparationTime = $preparationTime;
        $this->bulletpoints = $bulletpoints;
        $this->shelfLife = $shelfLife;
        $this->cuisine = $cuisine;
        $this->equipmentNeeded = $equipmentNeeded;
        $this->country = $country;
        $this->inYourBox = $inYourBox;
        $this->goustoReference = $goustoReference;
        $this->rating = $rating;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function hasId(): bool
    {
        return (bool)$this->id;
    }

    /**
     * @return Rating
     */
    public function rating(): Rating
    {
        return $this->rating;
    }

    /**
     * @return int
     */
    public function goustoReference(): int
    {
        return $this->goustoReference;
    }

    /**
     * @return string
     */
    public function slug(): string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @param int $rating
     * @throws Exceptions\InvalidRateRangeException
     */
    public function rate(int $rating): void
    {
        $this->rating = new Rating($rating);
    }

    public function boxType(): BoxType
    {
        return $this->boxType;
    }

    public function shortTitle(): string
    {
        return $this->shortTitle;
    }

    public function marketingDescription(): string
    {
        return $this->marketingDescription;
    }

    public function calories(): Calories
    {
        return $this->calories;
    }

    public function carbs(): Carbs
    {
        return $this->carbs;
    }

    public function fat(): Fat
    {
        return $this->fat;
    }

    public function protein(): Protein
    {
        return $this->protein;
    }

    public function dietType(): DietType
    {
        return $this->dietType;
    }

    public function season(): string
    {
        return $this->season;
    }

    public function base(): string
    {
        return $this->base;
    }

    public function preparationTime(): PreparationTime
    {
        return $this->preparationTime;
    }

    public function bulletpoints(): BulletpointCollection
    {
        return $this->bulletpoints;
    }

    public function shelfLife(): ShelfLife
    {
        return $this->shelfLife;
    }

    public function cuisine(): Cuisine
    {
        return $this->cuisine;
    }

    public function equipmentNeeded(): string
    {
        return $this->equipmentNeeded;
    }

    public function country(): Country
    {
        return $this->country;
    }

    public function inYourBox(): InYourBox
    {
        return $this->inYourBox;
    }

    public function updatedAt(): Carbon
    {
        return $this->updatedAt;
    }
}
