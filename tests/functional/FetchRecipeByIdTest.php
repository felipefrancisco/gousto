<?php

namespace Tests\Functional;

use Test\TestCase;

class FetchRecipeByIdTest extends TestCase
{
    /** @test */
    public function itSucceedsToGetRecipe()
    {
        $this->get("recipes/1");

        $this->seeStatusCode(200);

        $this->seeJson([
            'data' => array (
                'id' => 1,
                'created_at' => [
                    'date' => '2015-06-30 17:58:00.000000',
                    'timezone_type' => 3,
                    'timezone' => 'UTC',
                ],
                'updated_at' => [
                    'date' => '2015-06-30 17:58:00.000000',
                    'timezone_type' => 3,
                    'timezone' => 'UTC',
                ],
                'box_type' => 'vegetarian',
                'title' => 'Sweet Chilli and Lime Beef on a Crunchy Fresh Noodle Salad',
                'slug' => 'sweet-chilli-and-lime-beef-on-a-crunchy-fresh-noodle-salad',
                'short_title' => '',
                'marketing_description' => 'Here we\'ve used onglet steak which is an extra flavoursome cut of beef that should never be cooked past medium rare. So if you\'re a fan of well done steak, this one may not be for you. However, if you love rare steak and fancy trying a new cut, please be',
                'calories_kcal' => 401,
                'protein_grams' => 12,
                'fat_grams' => 35,
                'carbs_grams' => 0,
                'bulletpoint1' => '',
                'bulletpoint2' => '',
                'bulletpoint3' => '',
                'recipe_diet_type_id' => 'meat',
                'season' => 'all',
                'base' => 'noodles',
                'protein_source' => 'beef',
                'preparation_time_minutes' => 35,
                'shelf_life_days' => 4,
                'equipment_needed' => 'Appetite',
                'origin_country' => 'Great Britain',
                'recipe_cuisine' => 'asian',
                'in_your_box' => '',
                'gousto_reference' => 59,
                'rate' => 1,
            ),
        ]);
    }

    /** @test */
    public function itFailsWhenRecipeDoesntExist()
    {
        $this->get("recipes/2");

        $this->seeStatusCode(404);

        $this->seeJson([
            'messages' => [
                'Recipe `2` was not found'
            ]
        ]);
    }
}
