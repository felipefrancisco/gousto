<?php

namespace Domain\Behaviours;

use Illuminate\Support\Collection;

trait RecipeSerialization
{
    /**
     * @return Collection
     */
    public function toCollection(): Collection
    {
        return collect([
            'id' => $this->id,
            'created_at' => $this->createdAt,
            'updated_at' => $this->updatedAt,
            'box_type' => $this->boxType->getValue(),
            'title' => $this->title,
            'slug' => $this->slug,
            'short_title' => $this->shortTitle,
            'marketing_description' => $this->marketingDescription,
            'calories_kcal' => $this->calories->asKcal(),
            'protein_grams' => $this->protein->asGrams(),
            'fat_grams' => $this->fat->asGrams(),
            'carbs_grams' => $this->carbs->asGrams(),
            'bulletpoint1' => $this->bulletpoints->first(),
            'bulletpoint2' => $this->bulletpoints->second(),
            'bulletpoint3' => $this->bulletpoints->third(),
            'recipe_diet_type_id' => $this->dietType->getValue(),
            'season' => $this->season,
            'base' => $this->base,
            'protein_source' => $this->protein->source()->getValue(),
            'preparation_time_minutes' => $this->preparationTime->inMinutes(),
            'shelf_life_days' => $this->shelfLife->inDays(),
            'equipment_needed' => $this->equipmentNeeded,
            'origin_country' => $this->country->name(),
            'recipe_cuisine' => $this->cuisine->getValue(),
            'in_your_box' => $this->inYourBox->toString(),
            'gousto_reference' => $this->goustoReference,
            'rate' => $this->rating
        ]);
    }

    /**
     * @return array
     */
    public function toCsv(): array
    {
        $collection = $this->toCollection();

        $collection->put('created_at', $this->createdAt->format('d/m/Y H:i:s'));

        if ($this->updatedAt) {
            $collection->put('updated_at', $this->updatedAt->format('d/m/Y H:i:s'));
        }

        return $collection->toArray();
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->toCollection()->toArray();
    }
}