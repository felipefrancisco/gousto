<?php

namespace Application\Services;

use Application\UseCases\CreateRecipeUseCase;
use Domain\Mutations\RecipeMutation;
use Domain\Recipe;
use Domain\Repositories\RecipeRepositoryInterface;
use Infrastructure\Factories\RecipeFactory;
use Infrastructure\Repositories\Strategies\SelfRatingStrategy;

class CreateRecipeService
{
    /** @var RecipeRepositoryInterface  */
    protected $recipeRepository;

    /** @var RecipeFactory  */
    protected $recipeFactory;

    public function __construct(
        RecipeRepositoryInterface $recipeRepository,
        RecipeFactory $recipeFactory
    ) {
        $this->recipeRepository = $recipeRepository;
        $this->recipeFactory = $recipeFactory;
    }

    public function handle(CreateRecipeUseCase $createRecipeUseCase): Recipe
    {
        $recipe = $this->recipeFactory->makeFromCreateRecipeUseCase($createRecipeUseCase);

        $recipe = $this->recipeRepository->save($recipe);

        return $recipe;
    }
}
