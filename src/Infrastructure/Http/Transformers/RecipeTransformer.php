<?php

namespace Infrastructure\Http\Transformers;

use Domain\Recipe;
use League\Fractal\TransformerAbstract;

class RecipeTransformer extends TransformerAbstract
{
    public function transform(Recipe $recipe)
    {
        return $recipe->jsonSerialize();
    }
}
