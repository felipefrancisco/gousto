<?php declare(strict_types=1);

use Application\Services\FetchRecipeByIdService;
use Application\UseCases\FetchRecipeByIdUseCase;
use Application\Services\RateRecipeByIdService;
use Application\UseCases\RateRecipeByIdUseCase;
use Application\Services\UpdateRecipeByIdService;
use Application\UseCases\UpdateRecipeByIdUseCase;
use Application\Services\CreateRecipeService;
use Application\UseCases\CreateRecipeUseCase;
use Infrastructure\Http\Requests\FetchRecipeByIdRequest;
use Infrastructure\Http\Transformers\RecipeTransformer;
use Infrastructure\Http\Requests\RateRecipeByIdRequest;
use Infrastructure\Http\Requests\UpdateRecipeByIdRequest;
use Infrastructure\Http\Requests\CreateRecipeRequest;
use League\Fractal\Resource;
use League\Fractal;

$router->get('/recipes/{id}', function (
    FetchRecipeByIdRequest $request,
    RecipeTransformer $transformer,
    Fractal\Manager $fractal,
    $id
) use ($router) {

    $request['id'] = $id;

    $this->validate($request, $request->rules());

    $id = $request->getId();

    /** @var FetchRecipeByIdService $service */
    $service = app(FetchRecipeByIdService::class);
    $useCase = new FetchRecipeByIdUseCase($id);

    $recipe = $service->handle($useCase);

    $resource = new Resource\Item($recipe, $transformer);

    return response()->json(
        $fractal->createData($resource)->toArray(),
        200
    );
});

$router->post('/recipes/{id}/rate', function (
    RateRecipeByIdRequest $request,
    $id
) use ($router) {

    $request['id'] = $id;

    $this->validate($request, $request->rules());

    /** @var RateRecipeByIdService $service */
    $service = app(RateRecipeByIdService::class);
    $useCase = RateRecipeByIdUseCase::fromRequest($request);

    $service->handle($useCase);

    return response()->json(null, 200);
});

$router->put('/recipes/{id}', function (
    UpdateRecipeByIdRequest $request,
    RecipeTransformer $transformer,
    Fractal\Manager $fractal,
    $id
) use ($router) {

    $request['id'] = $id;

    $this->validate($request, $request->rules());

    /** @var UpdateRecipeByIdService $service */
    $service = app(UpdateRecipeByIdService::class);
    $useCase = UpdateRecipeByIdUseCase::fromRequest($request);

    $recipe = $service->handle($useCase);

    $resource = new Resource\Item($recipe, $transformer);

    return response()->json(
        $fractal->createData($resource)->toArray(),
        200
    );
});

$router->post('/recipes', function (
    CreateRecipeRequest $request,
    RecipeTransformer $transformer,
    Fractal\Manager $fractal
) use ($router) {

    $this->validate($request, $request->rules());

    /** @var CreateRecipeService $service */
    $service = app(CreateRecipeService::class);
    $useCase = CreateRecipeUseCase::fromRequest($request);

    $recipe = $service->handle($useCase);

    $resource = new Resource\Item($recipe, $transformer);

    return response()->json(
        $fractal->createData($resource)->toArray(),
        201
    );
});


