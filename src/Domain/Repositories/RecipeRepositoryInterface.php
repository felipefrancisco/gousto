<?php declare(strict_types=1);

namespace Domain\Repositories;

use Domain\Cuisine;
use Domain\Recipe;
use Illuminate\Support\Collection;

/**
 * Interface RecipeRepositoryInterface
 * @package Domain\Repositories
 */
interface RecipeRepositoryInterface
{
    /**
     * @param int $id
     * @return Recipe
     */
    public function findById(int $id): Recipe;


    /**
     * @param Recipe $recipe
     * @return Recipe
     */
    public function save(Recipe $recipe): Recipe;

    /**
     * @param Cuisine $cuisine
     * @return Collection
     */
    public function findByCuisine(Cuisine $cuisine): Collection;
}
