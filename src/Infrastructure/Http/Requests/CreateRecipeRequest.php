<?php declare(strict_types=1);

namespace Infrastructure\Http\Requests;

use Illuminate\Http\Request;

class CreateRecipeRequest extends Request
{
    public function rules()
    {
        return [
            "box_type" => ["required", 'string'],
            "title" => ["required", 'string'],
            "slug" => ["required", 'string'],
            "short_title" => ["present", 'string'],
            "marketing_description" => ["required", 'string'],
            "calories_kcal" => ["required", 'integer'],
            "protein_grams" => ["required", 'integer'],
            "fat_grams" => ["required", 'integer'],
            "carbs_grams" => ["required", 'integer'],
            "bulletpoint1" => ["present", 'string'],
            "bulletpoint2" => ["present", 'string'],
            "bulletpoint3" => ["present", 'string'],
            "recipe_diet_type_id" => ["required", 'string'],
            "season" => ["required", 'string'],
            "base" => ["required", 'string'],
            "protein_source" => ["required", 'string'],
            "preparation_time_minutes" => ["required", 'integer'],
            "shelf_life_days" => ["required", 'integer'],
            "equipment_needed" => ["required", 'string'],
            "origin_country" => ["required", 'string'],
            "recipe_cuisine" => ["required", 'string'],
            "in_your_box" => ["present", 'string'],
            "gousto_reference" => ["required", 'integer']
        ];
    }

    public function getId(): int
    {
        $id = collect($this->all())->get('id');

        return (int)($id);
    }
}