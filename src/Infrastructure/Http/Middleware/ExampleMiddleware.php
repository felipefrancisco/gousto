<?php declare(strict_types=1);

namespace Infrastructure\Http\Middleware;

use Closure;

/**
 * Class ExampleMiddleware
 * @package Infrastructure\Http\Middleware
 */
class ExampleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
