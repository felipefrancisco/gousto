<?php

namespace Application\Services;

use Application\UseCases\UpdateRecipeByIdUseCase;
use Domain\Mutations\RecipeMutation;
use Domain\Mutations\RecipeMutationDecorator;
use Domain\Recipe;
use Domain\Repositories\RecipeRepositoryInterface;
use Infrastructure\Repositories\Strategies\SelfRatingStrategy;

class UpdateRecipeByIdService
{
    /** @var RecipeRepositoryInterface  */
    protected $recipeRepository;

    public function __construct(
        RecipeRepositoryInterface $recipeRepository
    ) {
        $this->recipeRepository = $recipeRepository;
    }

    public function handle(UpdateRecipeByIdUseCase $updateRecipeByIdUseCase): Recipe
    {
        $recipe = $this->recipeRepository->findById(
            $updateRecipeByIdUseCase->getId()
        );

        $recipe = RecipeMutationDecorator::decorate($recipe);

        $recipe->mutateUsingUpdateRecipeByIdUseCase($updateRecipeByIdUseCase);

        $recipe = $this->recipeRepository->save($recipe);

        return $recipe;
    }
}
