<?php declare(strict_types=1);

namespace Domain;

use MabeEnum\Enum;

/**
 * Class DietType
 * @package Domain
 */
class DietType extends Enum
{
    public const MEAT = 'meat';
    public const FISH = 'fish';
    public const VEGETARIAN = 'vegetarian';
}
