<?php declare(strict_types=1);

namespace Domain\Exceptions;

/**
 * Class InvalidPreparationTimeException
 * @package Domain\Exceptions
 */
class InvalidPreparationTimeException extends \Exception
{
    protected $code = 4;

    /**
     * @param int $minutes
     * @return InvalidPreparationTimeException
     */
    public static function withMinutes(int $minutes): self
    {
        return new self(
            sprintf('Invalid Preparation Time `%d`. Expected greater or equal than 0.', $minutes)
        );
    }
}
