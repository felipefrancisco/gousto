<?php declare(strict_types=1);

namespace Domain;

use Domain\Exceptions\InvalidPreparationTimeException;

/**
 * Class PreparationTime
 * @package Domain
 */
class PreparationTime
{
    protected $minutes;

    /**
     * PreparationTime constructor.
     * @param int $minutes
     * @throws InvalidPreparationTimeException
     */
    public function __construct(int $minutes)
    {
        if ($minutes < 0) {
            throw InvalidPreparationTimeException::withMinutes($minutes);
        }

        $this->minutes = $minutes;
    }

    /**
     * @return int
     */
    public function inMinutes(): int
    {
        return $this->minutes;
    }

    /**
     * @return int
     */
    public function inHours(): int
    {
        return ($this->minutes / 60);
    }
}
