<?php

namespace Application\UseCases;

use Domain\Cuisine;
use Infrastructure\Http\Requests\FetchAllRecipesByCuisineRequest;

class FetchAllRecipesByCuisineUseCase
{
    /** @var Cuisine  */
    protected $cuisine;

    public function __construct(Cuisine $cuisine)
    {
        $this->cuisine = $cuisine;
    }

    public function getCuisine(): Cuisine
    {
        return $this->cuisine;
    }

    public static function fromRequest(FetchAllRecipesByCuisineRequest $request): FetchAllRecipesByCuisineUseCase
    {
        $cuisine = Cuisine::byValue(
            $request->getCuisine()
        );

        return new self(
            $cuisine
        );
    }
}
