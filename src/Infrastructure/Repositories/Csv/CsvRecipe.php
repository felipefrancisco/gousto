<?php declare(strict_types=1);

namespace Infrastructure\Repositories\Csv;

use Domain\Exceptions\InvalidRecipeCsvStructure;
use Illuminate\Support\Collection;

class CsvRecipe
{
    /** @var int  */
    protected $id;

    /** @var string  */
    protected $createdAt;

    /** @var string  */
    protected $updatedAt;

    /** @var string  */
    protected $boxType;

    /** @var string  */
    protected $title;

    /** @var string  */
    protected $slug;

    /** @var string|null  */
    protected $shortTitle;

    /** @var string  */
    protected $marketingDescription;

    /** @var int  */
    protected $caloriesKcal;

    /** @var int  */
    protected $proteinGrams;

    /** @var int  */
    protected $fatGrams;

    /** @var int  */
    protected $carbsGrams;

    /** @var string|null  */
    protected $bulletpoint1;

    /** @var string|null  */
    protected $bulletpoint2;

    /** @var string|null  */
    protected $bulletpoint3;

    /** @var string  */
    protected $recipeDietTypeId;

    /** @var string  */
    protected $season;

    /** @var string  */
    protected $base;

    /** @var string  */
    protected $proteinSource;

    /** @var int  */
    protected $preparationTimeMinutes;

    /** @var int  */
    protected $shelfLifeDays;

    /** @var string  */
    protected $equipmentNeeded;

    /** @var string  */
    protected $originCountry;

    /** @var string  */
    protected $recipeCuisine;

    /** @var string  */
    protected $inYourBox;

    /** @var int  */
    protected $goustoReference;

    /** @var int */
    protected $rating;

    public function __construct(
        int $id,
        string $createdAt,
        string $updatedAt,
        string $boxType,
        string $title,
        string $slug,
        ?string $shortTitle,
        string $marketingDescription,
        int $caloriesKcal,
        int $proteinGrams,
        int $fatGrams,
        int $carbsGrams,
        ?string $bulletpoint1,
        ?string $bulletpoint2,
        ?string $bulletpoint3,
        string $recipeDietTypeId,
        string $season,
        string $base,
        string $proteinSource,
        int $preparationTimeMinutes,
        int $shelfLifeDays,
        string $equipmentNeeded,
        string $originCountry,
        string $recipeCuisine,
        string $inYourBox,
        int $goustoReference,
        ?int $rating
    ) {
         $this->id = $id;
         $this->createdAt = $createdAt;
         $this->updatedAt = $updatedAt;
         $this->boxType = $boxType;
         $this->title = $title;
         $this->slug = $slug;
         $this->shortTitle = $shortTitle;
         $this->marketingDescription = $marketingDescription;
         $this->caloriesKcal = $caloriesKcal;
         $this->proteinGrams = $proteinGrams;
         $this->fatGrams = $fatGrams;
         $this->carbsGrams = $carbsGrams;
         $this->bulletpoint1 = $bulletpoint1;
         $this->bulletpoint2 = $bulletpoint2;
         $this->bulletpoint3 = $bulletpoint3;
         $this->recipeDietTypeId = $recipeDietTypeId;
         $this->season = $season;
         $this->base = $base;
         $this->proteinSource = $proteinSource;
         $this->preparationTimeMinutes = $preparationTimeMinutes;
         $this->shelfLifeDays = $shelfLifeDays;
         $this->equipmentNeeded = $equipmentNeeded;
         $this->originCountry = $originCountry;
         $this->recipeCuisine = $recipeCuisine;
         $this->inYourBox = $inYourBox;
         $this->goustoReference = $goustoReference;
         $this->rating = $rating;
    }

    /**
     * @return int
     */
    public function id(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function createdAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @return string
     */
    public function updatedAt(): ?string
    {
        return $this->updatedAt ?: null;
    }

    /**
     * @return string
     */
    public function boxType(): string
    {
        return $this->boxType;
    }

    /**
     * @return string
     */
    public function title(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function slug(): string
    {
        return $this->slug;
    }

    /**
     * @return string|null
     */
    public function shortTitle(): ?string
    {
        return $this->shortTitle;
    }

    /**
     * @return string
     */
    public function marketingDescription(): string
    {
        return $this->marketingDescription;
    }

    /**
     * @return int
     */
    public function caloriesKcal(): int
    {
        return $this->caloriesKcal;
    }

    /**
     * @return int
     */
    public function proteinGrams(): int
    {
        return $this->proteinGrams;
    }

    /**
     * @return int
     */
    public function fatGrams(): int
    {
        return $this->fatGrams;
    }

    /**
     * @return int
     */
    public function carbsGrams(): int
    {
        return $this->carbsGrams;
    }

    /**
     * @return string|null
     */
    public function bulletpoint1(): ?string
    {
        return $this->bulletpoint1;
    }

    /**
     * @return string|null
     */
    public function bulletpoint2(): ?string
    {
        return $this->bulletpoint2;
    }

    /**
     * @return string|null
     */
    public function bulletpoint3(): ?string
    {
        return $this->bulletpoint3;
    }

    /**
     * @return string
     */
    public function recipeDietTypeId(): string
    {
        return $this->recipeDietTypeId;
    }

    /**
     * @return string
     */
    public function season(): string
    {
        return $this->season;
    }

    /**
     * @return string
     */
    public function base(): string
    {
        return $this->base;
    }

    /**
     * @return string
     */
    public function proteinSource(): string
    {
        return $this->proteinSource;
    }

    /**
     * @return int
     */
    public function preparationTimeMinutes(): int
    {
        return $this->preparationTimeMinutes;
    }

    /**
     * @return int
     */
    public function shelfLifeDays(): int
    {
        return $this->shelfLifeDays;
    }

    /**
     * @return string
     */
    public function equipmentNeeded(): string
    {
        return $this->equipmentNeeded;
    }

    /**
     * @return string
     */
    public function originCountry(): string
    {
        return $this->originCountry;
    }

    /**
     * @return string
     */
    public function recipeCuisine(): string
    {
        return $this->recipeCuisine;
    }

    /**
     * @return array
     */
    public function inYourBox(): array
    {
        if (!$this->inYourBox) {
            return [];
        }

        return explode(', ', $this->inYourBox);
    }

    /**
     * @return int
     */
    public function goustoReference(): int
    {
        return $this->goustoReference;
    }

    public function rating(): ?int
    {
        return $this->rating;
    }

    /**
     * @param Collection $collection
     * @return CsvRecipe
     * @throws InvalidRecipeCsvStructure
     */
    public static function fromCollection(Collection $collection)
    {
        try {

            $rating = (int)$collection->get('rating');

            if ($rating === 0) {
                $rating = null;
            }

            return new self(
                (int)($collection->get('id')),
                $collection->get('created_at'),
                $collection->get('updated_at'),
                $collection->get('box_type'),
                $collection->get('title'),
                $collection->get('slug'),
                $collection->get('short_title'),
                $collection->get('marketing_description'),
                (int)($collection->get('calories_kcal')),
                (int)($collection->get('protein_grams')),
                (int)($collection->get('fat_grams')),
                (int)($collection->get('carbs_grams')),
                $collection->get('bulletpoint1'),
                $collection->get('bulletpoint2'),
                $collection->get('bulletpoint3'),
                $collection->get('recipe_diet_type_id'),
                $collection->get('season'),
                $collection->get('base'),
                $collection->get('protein_source'),
                (int)($collection->get('preparation_time_minutes')),
                (int)($collection->get('shelf_life_days')),
                $collection->get('equipment_needed'),
                $collection->get('origin_country'),
                $collection->get('recipe_cuisine'),
                $collection->get('in_your_box'),
                (int)($collection->get('gousto_reference')),
                $rating
            );
        }
        catch (\TypeError $e) {
            dd($e);
            throw InvalidRecipeCsvStructure::withId(
                (int)($collection->get('id'))
            );
        }
    }
}
