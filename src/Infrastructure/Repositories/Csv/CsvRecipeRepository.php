<?php declare(strict_types=1);

namespace Infrastructure\Repositories\Csv;

use Carbon\Carbon;
use Domain\Cuisine;
use Domain\Mutations\RecipeMutationDecorator;
use Domain\Recipe;
use Domain\Repositories\RecipeRepositoryInterface;
use Illuminate\Support\Collection;
use Infrastructure\Factories\RecipeFactory;
use League\Csv\Reader;
use League\Csv\Writer;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CsvRecipeRepository implements RecipeRepositoryInterface
{
    /** @var Reader  */
    protected $reader;

    /** @var Writer  */
    protected $writer;

    /** @var Collection  */
    protected $records;

    /** @var RecipeFactory  */
    protected $RecipeFactory;

    /**
     * CsvRecipeRepository constructor.
     * @param Reader $reader
     * @param Writer $writer
     * @param RecipeFactory $RecipeFactory
     * @throws \Exception
     */
    public function __construct(
        Reader $reader,
        Writer $writer,
        RecipeFactory $RecipeFactory
    ) {
        $this->reader = $reader;
        $this->writer = $writer;
        $this->RecipeFactory = $RecipeFactory;
        $this->records = $this->recordsToCollection(
            $reader->getRecords()
        );
    }

    /**
     * @param int $id
     * @return Recipe
     */
    public function findById(int $id): Recipe
    {
        $record = $this->records->get($id);

        if (!$record) {
            throw new NotFoundHttpException(
                sprintf('Recipe `%d` was not found', $id)
            );
        }

        return $record;
    }

    public function save(Recipe $recipe): Recipe
    {
        if ($recipe->hasId()) {
            $recipe = $this->updateUpdatedAt($recipe);
        }

        if (!$recipe->hasId()) {
            $recipe = $this->addIdTo($recipe);
        }

        $this->records->put(
            $recipe->id(),
            $recipe
        );

        $this->persist();

        return $recipe;
    }

    /**
     * @throws \League\Csv\CannotInsertRecord
     */
    private function persist(): void
    {
        $writer = $this->writer;

        $writer->insertOne(
            $this->reader->getHeader()
        );

        $this->records->each(function(Recipe $recipe) use ($writer) {
            $writer->insertOne(
                $recipe->toCsv()
            );
        });

        return;
    }

    public function findByCuisine(Cuisine $cuisine): Collection
    {
        return $this->records->filter(function (Recipe $recipe) use ($cuisine) {
            return ($recipe->cuisine() === $cuisine);
        });
    }

    /**
     * @param \Iterator $records
     * @return Collection
     * @throws \Exception
     */
    private function recordsToCollection(\Iterator $records): Collection
    {
        $collection = collect();

        foreach ($records as $key => $record) {

            $csvRecipe = CsvRecipe::fromCollection(
                collect($record)
            );

            $recipe = $this->RecipeFactory->makeFromCsv(
                $csvRecipe
            );

            $collection->put($recipe->id(), $recipe);
        }

        return $collection;
    }

    private function updateUpdatedAt(Recipe $recipe): Recipe
    {
        return RecipeMutationDecorator::decorate($recipe)->mutateUsingUpdatedAt(
            Carbon::now()
        );
    }

    private function addIdTo(Recipe $recipe): Recipe
    {
        $id = 1;

        if ($this->records->last()) {
            $id = $this->records->last()->id();
            $id++;
        }

        return RecipeMutationDecorator::decorate($recipe)->mutateUsingId($id);
    }

    public function records(): Collection
    {
        return $this->records;
    }
}
