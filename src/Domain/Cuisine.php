<?php declare(strict_types=1);

namespace Domain;

use MabeEnum\Enum;

/**
 * Class Cuisine
 * @package Domain
 */
class Cuisine extends Enum
{
    public const ASIAN = 'asian';
    public const ITALIAN = 'italian';
    public const BRITISH = 'british';
    public const MEDITERRANEAN = 'mediterranean';
    public const MEXICAN = 'mexican';
}
