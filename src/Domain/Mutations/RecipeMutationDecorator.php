<?php

namespace Domain\Mutations;

use Application\UseCases\UpdateRecipeByIdUseCase;
use Carbon\Carbon;
use Domain\Recipe;

class RecipeMutationDecorator extends Recipe
{
    public static function decorate(Recipe $recipe): self
    {
        return new self(
            $recipe->id,
            $recipe->boxType,
            $recipe->title,
            $recipe->slug,
            $recipe->shortTitle,
            $recipe->marketingDescription,
            $recipe->calories,
            $recipe->carbs,
            $recipe->fat,
            $recipe->protein,
            $recipe->dietType,
            $recipe->season,
            $recipe->base,
            $recipe->preparationTime,
            $recipe->bulletpoints,
            $recipe->shelfLife,
            $recipe->cuisine,
            $recipe->equipmentNeeded,
            $recipe->country,
            $recipe->inYourBox,
            $recipe->goustoReference,
            $recipe->rating,
            $recipe->createdAt,
            $recipe->updatedAt
        );
    }

    public function mutateUsingUpdateRecipeByIdUseCase(UpdateRecipeByIdUseCase $updateRecipeByIdUseCase): self
    {
        $this->boxType = $updateRecipeByIdUseCase->boxType();
        $this->title = $updateRecipeByIdUseCase->title();
        $this->slug = $updateRecipeByIdUseCase->slug();
        $this->shortTitle = $updateRecipeByIdUseCase->shortTitle();
        $this->marketingDescription = $updateRecipeByIdUseCase->marketingDescription();
        $this->calories = $updateRecipeByIdUseCase->calories();
        $this->carbs = $updateRecipeByIdUseCase->carbs();
        $this->fat = $updateRecipeByIdUseCase->fat();
        $this->protein = $updateRecipeByIdUseCase->protein();
        $this->dietType = $updateRecipeByIdUseCase->dietType();
        $this->season = $updateRecipeByIdUseCase->season();
        $this->base = $updateRecipeByIdUseCase->base();
        $this->preparationTime = $updateRecipeByIdUseCase->preparationTime();
        $this->bulletpoints = $updateRecipeByIdUseCase->bulletpoints();
        $this->shelfLife = $updateRecipeByIdUseCase->shelfLife();
        $this->cuisine = $updateRecipeByIdUseCase->cuisine();
        $this->equipmentNeeded = $updateRecipeByIdUseCase->equipmentNeeded();
        $this->country = $updateRecipeByIdUseCase->country();
        $this->inYourBox = $updateRecipeByIdUseCase->inYourBox();
        $this->goustoReference = $updateRecipeByIdUseCase->goustoReference();
        $this->updatedAt = $updateRecipeByIdUseCase->updatedAt();

        return $this;
    }

    public function mutateUsingUpdatedAt(Carbon $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function mutateUsingId(int $id): self
    {
        $this->id = $id;

        return $this;
    }
}