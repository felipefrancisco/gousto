<?php declare(strict_types=1);

namespace Domain\Exceptions;

/**
 * Class InvalidRecipeCsvStructure
 * @package Domain\Exceptions
 */
class InvalidRecipeCsvStructure extends \Exception
{
    protected $code = 6;

    public static function withId(int $id): self
    {
        return new self(
            sprintf('Invalid Recipe CSV structure with id `%d`. Please check the csv storage.', $id)
        );
    }
}
