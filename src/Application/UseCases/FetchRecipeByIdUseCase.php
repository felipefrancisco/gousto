<?php

namespace Application\UseCases;

use Infrastructure\Http\Requests\FetchRecipeByIdRequest;

class FetchRecipeByIdUseCase
{
    /** @var int  */
    protected $id;

    /**
     * FetchRecipeByIdUseCase constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public static function fromRequest(FetchRecipeByIdRequest $request): FetchRecipeByIdUseCase
    {
        return new self(
            $request->getId()
        );
    }
}
