<?php

namespace Tests\Functional;

use Domain\Repositories\RecipeRepositoryInterface;
use Illuminate\Http\Request;
use Infrastructure\Http\Requests\CreateRecipeRequest;
use Infrastructure\Http\Requests\RateRecipeByIdRequest;
use Infrastructure\Http\Requests\UpdateRecipeByIdRequest;
use Infrastructure\Repositories\Csv\CsvRecipeRepository;
use Test\TestCase;

class FetchAllRecipesByCuisineTest extends TestCase
{
    /** @var RecipeRepositoryInterface */
    protected $recipeRepository;

    public function setUp()
    {
        parent::setUp();

        $this->recipeRepository = app(CsvRecipeRepository::class);
    }

    /** @test */
    public function itSucceedsToFetchCorrectRecipes(): void
    {
        $response = $this->json('GET', '/cuisines/asian/recipes', [
            'page' => 1
        ])->response->getContent();

        $this->seeStatusCode(200);

        $response = json_decode($response);
        $data = $response->data;

        $this->assertCount(1, $data);

        $response = $this->json('GET', '/cuisines/italian/recipes', [
            'page' => 1
        ])->response->getContent();

        $this->seeStatusCode(200);

        $response = json_decode($response);
        $data = $response->data;

        $this->assertCount(0, $data);
    }
}
