<?php declare(strict_types=1);

namespace Infrastructure\Providers;

use Application\Services\CreateRecipeService;
use Application\Services\FetchRecipeByIdService;
use Application\Services\FetchRecipeByIdUseCase;
use Application\Services\RateRecipeByIdService;
use Application\Services\UpdateRecipeByIdService;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Factories\RecipeFactory;
use Infrastructure\Http\Requests\CreateRecipeRequest;
use Infrastructure\Http\Requests\FetchAllRecipesByCuisineRequest;
use Infrastructure\Http\Requests\FetchRecipeByIdRequest;
use Infrastructure\Http\Requests\RateRecipeByIdRequest;
use Infrastructure\Http\Requests\UpdateRecipeByIdRequest;
use Infrastructure\Repositories\Csv\CsvRecipeRepository;

/**
 * Class RequestServiceProvider
 * @package Infrastructure\Providers
 */
class RequestServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CreateRecipeRequest::class, function () {
            return CreateRecipeRequest::createFrom(
                app(Request::class)
            );
        });

        $this->app->bind(UpdateRecipeByIdRequest::class, function () {
            return UpdateRecipeByIdRequest::createFrom(
                app(Request::class)
            );
        });

        $this->app->bind(RateRecipeByIdRequest::class, function () {
            return RateRecipeByIdRequest::createFrom(
                app(Request::class)
            );
        });

        $this->app->bind(FetchRecipeByIdRequest::class, function () {
            return FetchRecipeByIdRequest::createFrom(
                app(Request::class)
            );
        });

        $this->app->bind(FetchAllRecipesByCuisineRequest::class, function () {
            return FetchAllRecipesByCuisineRequest::createFrom(
                app(Request::class)
            );
        });
    }
}
