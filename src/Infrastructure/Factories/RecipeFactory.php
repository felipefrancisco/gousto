<?php declare(strict_types=1);

namespace Infrastructure\Factories;

use Application\UseCases\CreateRecipeUseCase;
use Carbon\Carbon;
use Domain\BoxType;
use Domain\BulletpointCollection;
use Domain\Calories;
use Domain\Carbs;
use Domain\Country;
use Domain\Cuisine;
use Domain\DietType;
use Domain\Fat;
use Domain\InYourBox;
use Domain\PreparationTime;
use Domain\Protein;
use Domain\ProteinSource;
use Domain\Rating;
use Domain\Recipe;
use Domain\ShelfLife;
use Infrastructure\Repositories\Csv\CsvRecipe;

class RecipeFactory
{
    private function make(
        ?int $id,
        string $createdAt,
        ?string $updatedAt,
        string $boxType,
        string $title,
        string $slug,
        ?string $shortTitle,
        string $marketingDescription,
        int $caloriesKcal,
        int $proteinGrams,
        int $fatGrams,
        int $carbsGrams,
        ?string $bulletpoint1,
        ?string $bulletpoint2,
        ?string $bulletpoint3,
        string $recipeDietTypeId,
        string $season,
        string $base,
        string $proteinSource,
        int $preparationTimeMinutes,
        int $shelfLifeDays,
        string $equipmentNeeded,
        string $originCountry,
        string $recipeCuisine,
        array $inYourBox,
        int $goustoReference,
        ?int $rating
    ) {
        $boxType = BoxType::byValue(
            $boxType
        );

        $calories = new Calories(
            ($caloriesKcal * 1000)
        );

        $carbs = new Carbs(
            $carbsGrams
        );

        $fat = new Fat(
            $fatGrams
        );

        $protein = new Protein(
            $proteinGrams,
            ProteinSource::byValue(
                $proteinSource
            )
        );

        $dietType = DietType::byValue(
            $recipeDietTypeId
        );

        $preparationTime = new PreparationTime(
            $preparationTimeMinutes
        );

        $bulletpoints = new BulletpointCollection([
            $bulletpoint1,
            $bulletpoint2,
            $bulletpoint3,
        ]);

        $shelfLife = new ShelfLife(
            $shelfLifeDays
        );

        $cuisine = Cuisine::byValue(
            $recipeCuisine
        );

        $country = new Country(
            $originCountry
        );

        $inYourBox = new InYourBox(
            $inYourBox
        );

        $createdAt = Carbon::createFromFormat('d/m/Y H:i:s', $createdAt);

        if ($updatedAt) {
            $updatedAt = Carbon::createFromFormat('d/m/Y H:i:s', $updatedAt);
        }

        if ($rating) {
            $rating = new Rating($rating);
        }

        return new Recipe(
            $id,
            $boxType,
            $title,
            $slug,
            $shortTitle,
            $marketingDescription,
            $calories,
            $carbs,
            $fat,
            $protein,
            $dietType,
            $season,
            $base,
            $preparationTime,
            $bulletpoints,
            $shelfLife,
            $cuisine,
            $equipmentNeeded,
            $country,
            $inYourBox,
            $goustoReference,
            $rating,
            $createdAt,
            $updatedAt
        );
    }

   public function makeFromCreateRecipeUseCase(CreateRecipeUseCase $useCase): Recipe
   {
       return $this->make(
           null,
           Carbon::now()->format('d/m/Y H:i:s'),
           null,
           $useCase->boxType()->getValue(),
           $useCase->title(),
           $useCase->slug(),
           $useCase->shortTitle(),
           $useCase->marketingDescription(),
           $useCase->calories()->asKcal(),
           $useCase->protein()->asGrams(),
           $useCase->fat()->asGrams(),
           $useCase->carbs()->asGrams(),
           $useCase->bulletpoints()->first(),
           $useCase->bulletpoints()->second(),
           $useCase->bulletpoints()->third(),
           $useCase->dietType()->getValue(),
           $useCase->season(),
           $useCase->base(),
           $useCase->protein()->source()->getValue(),
           $useCase->preparationTime()->inMinutes(),
           $useCase->shelfLife()->inDays(),
           $useCase->equipmentNeeded(),
           $useCase->country()->name(),
           $useCase->cuisine()->getValue(),
           $useCase->inYourBox()->toArray(),
           $useCase->goustoReference(),
           null
       );
   }

   public function makeFromCsv(CsvRecipe $csvRecipe): Recipe
   {
       return $this->make(
            $csvRecipe->id(),
            $csvRecipe->createdAt(),
            $csvRecipe->updatedAt(),
            $csvRecipe->boxType(),
            $csvRecipe->title(),
            $csvRecipe->slug(),
            $csvRecipe->shortTitle(),
            $csvRecipe->marketingDescription(),
            $csvRecipe->caloriesKcal(),
            $csvRecipe->proteinGrams(),
            $csvRecipe->fatGrams(),
            $csvRecipe->carbsGrams(),
            $csvRecipe->bulletpoint1(),
            $csvRecipe->bulletpoint2(),
            $csvRecipe->bulletpoint3(),
            $csvRecipe->recipeDietTypeId(),
            $csvRecipe->season(),
            $csvRecipe->base(),
            $csvRecipe->proteinSource(),
            $csvRecipe->preparationTimeMinutes(),
            $csvRecipe->shelfLifeDays(),
            $csvRecipe->equipmentNeeded(),
            $csvRecipe->originCountry(),
            $csvRecipe->recipeCuisine(),
            $csvRecipe->inYourBox(),
            $csvRecipe->goustoReference(),
            $csvRecipe->rating()
       );
   }
}
