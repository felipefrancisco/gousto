<?php

namespace Application\UseCases;

use Carbon\Carbon;
use Domain\BoxType;
use Domain\BulletpointCollection;
use Domain\Calories;
use Domain\Carbs;
use Domain\Country;
use Domain\Cuisine;
use Domain\DietType;
use Domain\Fat;
use Domain\InYourBox;
use Domain\PreparationTime;
use Domain\Protein;
use Domain\ProteinSource;
use Domain\ShelfLife;
use Illuminate\Support\Collection;
use Infrastructure\Http\Requests\UpdateRecipeByIdRequest;

class UpdateRecipeByIdUseCase
{
    /** @var int  */
    protected $id;

    /** @var BoxType  */
    protected $boxType;

    /** @var string  */
    protected $title;

    /** @var string  */
    protected $slug;

    /** @var string  */
    protected $shortTitle;

    /** @var string  */
    protected $marketingDescription;

    /** @var Calories  */
    protected $calories;

    /** @var Carbs  */
    protected $carbs;

    /** @var Fat  */
    protected $fat;

    /** @var Protein  */
    protected $protein;

    /** @var DietType  */
    protected $dietType;

    /** @var string  */
    protected $season;

    /** @var string  */
    protected $base;

    /** @var PreparationTime  */
    protected $preparationTime;

    /** @var Collection  */
    protected $bulletpoints;

    /** @var ShelfLife  */
    protected $shelfLife;

    /** @var Cuisine  */
    protected $cuisine;

    /** @var string  */
    protected $equipmentNeeded;

    /** @var Country  */
    protected $country;

    /** @var InYourBox  */
    protected $inYourBox;

    /** @var int  */
    protected $goustoReference;

    /** @var Carbon  */
    protected $updatedAt;

    public function __construct(
        int $id,
        BoxType $boxType,
        string $title,
        string $slug,
        string $shortTitle,
        string $marketingDescription,
        Calories $calories,
        Carbs $carbs,
        Fat $fat,
        Protein $protein,
        DietType $dietType,
        string $season,
        string $base,
        PreparationTime $preparationTime,
        Collection $bulletpoints,
        ShelfLife $shelfLife,
        Cuisine $cuisine,
        string $equipmentNeeded,
        Country $country,
        InYourBox $inYourBox,
        int $goustoReference
    ) {
        $this->id = $id;
        $this->boxType = $boxType;
        $this->title = $title;
        $this->slug = $slug;
        $this->shortTitle = $shortTitle;
        $this->marketingDescription = $marketingDescription;
        $this->calories = $calories;
        $this->carbs = $carbs;
        $this->fat = $fat;
        $this->protein = $protein;
        $this->dietType = $dietType;
        $this->season = $season;
        $this->base = $base;
        $this->preparationTime = $preparationTime;
        $this->bulletpoints = $bulletpoints;
        $this->shelfLife = $shelfLife;
        $this->cuisine = $cuisine;
        $this->equipmentNeeded = $equipmentNeeded;
        $this->country = $country;
        $this->inYourBox = $inYourBox;
        $this->goustoReference = $goustoReference;
        $this->updatedAt = Carbon::now();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    public function boxType(): BoxType
    {
        return $this->boxType;
    }

    public function title(): string
    {
        return $this->title;
    }

    public function slug(): string
    {
        return $this->slug;
    }

    public function shortTitle(): string
    {
        return $this->shortTitle;
    }

    public function marketingDescription(): string
    {
        return $this->marketingDescription;
    }

    public function calories(): Calories
    {
        return $this->calories;
    }

    public function carbs(): Carbs
    {
        return $this->carbs;
    }

    public function fat(): Fat
    {
        return $this->fat;
    }

    public function protein(): Protein
    {
        return $this->protein;
    }

    public function dietType(): DietType
    {
        return $this->dietType;
    }

    public function season(): string
    {
        return $this->season;
    }

    public function base(): string
    {
        return $this->base;
    }

    public function preparationTime(): PreparationTime
    {
        return $this->preparationTime;
    }

    public function bulletpoints(): BulletpointCollection
    {
        return $this->bulletpoints;
    }

    public function shelfLife(): ShelfLife
    {
        return $this->shelfLife;
    }

    public function cuisine(): Cuisine
    {
        return $this->cuisine;
    }

    public function equipmentNeeded(): string
    {
        return $this->equipmentNeeded;
    }

    public function country(): Country
    {
        return $this->country;
    }

    public function inYourBox(): InYourBox
    {
        return $this->inYourBox;
    }

    public function goustoReference(): int
    {
        return $this->goustoReference;
    }

    public function updatedAt(): Carbon
    {
        return $this->updatedAt;
    }

    public static function fromRequest(UpdateRecipeByIdRequest $request): UpdateRecipeByIdUseCase
    {
        $boxType = BoxType::byValue(
            $request->get('box_type')
        );

        $calories = new Calories(
            ($request->get('calories_kcal') * 1000)
        );

        $carbs = new Carbs(
            $request->get('carbs_grams')
        );

        $fat = new Fat(
            $request->get('fat_grams')
        );

        $protein = new Protein(
            $request->get('protein_grams'),
            ProteinSource::byValue(
                $request->get('protein_source')
            )
        );

        $dietType = DietType::byValue(
            $request->get('recipe_diet_type_id')
        );

        $preparationTime = new PreparationTime(
            $request->get('preparation_time_minutes')
        );

        $bulletpoints = new BulletpointCollection([
            $request->get('bulletpoint1'),
            $request->get('bulletpoint2'),
            $request->get('bulletpoint3'),
        ]);

        $shelfLife = new ShelfLife(
            $request->get('shelf_life_days')
        );

        $cuisine = Cuisine::byValue(
            $request->get('recipe_cuisine')
        );

        $country = new Country(
            $request->get('origin_country')
        );

        $inYourBox = new InYourBox(
            explode(", ", $request->get('in_your_box'))
        );

        return new self(
            $request->getId(),
            $boxType,
            $request->get('title'),
            $request->get('slug'),
            $request->get('short_title'),
            $request->get('marketing_description'),
            $calories,
            $carbs,
            $fat,
            $protein,
            $dietType,
            $request->get('season'),
            $request->get('base'),
            $preparationTime,
            $bulletpoints,
            $shelfLife,
            $cuisine,
            $request->get('equipment_needed'),
            $country,
            $inYourBox,
            $request->get('gousto_reference')
        );
    }
}
