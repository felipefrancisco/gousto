<?php declare(strict_types=1);

use Illuminate\Pagination\LengthAwarePaginator;
use Infrastructure\Http\Requests\FetchAllRecipesByCuisineRequest;
use Application\Services\FetchAllRecipesByCuisineService;
use Application\UseCases\FetchAllRecipesByCuisineUseCase;
use Infrastructure\Http\Transformers\RecipeTransformer;
use League\Fractal\Resource;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

$router->get('/cuisines/{cuisine}/recipes', function (
    FetchAllRecipesByCuisineRequest $request,
    RecipeTransformer $transformer,
    Manager $fractal,
    $cuisine
) use ($router) {

    $request['cuisine'] = $cuisine;
    
    $this->validate($request, $request->rules());

    /** @var FetchAllRecipesByCuisineService $service */
    $service = app(FetchAllRecipesByCuisineService::class);
    $useCase = FetchAllRecipesByCuisineUseCase::fromRequest($request);

    $recipes = $service->handle($useCase);

    $page = $request->getPage();
    $limit = $request->getLimit();

    $paginator = new LengthAwarePaginator(
        $recipes->forPage($page, $limit),
        $recipes->count(),
        $limit,
        $page
    );

    $resource = new Resource\Collection($recipes, $transformer);
    $resource->setPaginator(new IlluminatePaginatorAdapter($paginator));

    return response()->json(
        $fractal->createData($resource)->toArray(),
        200
    );
});
