<?php declare(strict_types=1);

namespace Domain;

/**
 * Class Country
 * @package Domain
 */
class Country
{
    protected $name;

    /**
     * Country constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }
}
