<?php

namespace Application\Services;

use Application\UseCases\FetchRecipeByIdUseCase;
use Domain\Recipe;
use Domain\Repositories\RecipeRepositoryInterface;

class FetchRecipeByIdService
{
    protected $recipeRepository;

    public function __construct(
        RecipeRepositoryInterface $recipeRepository
    ) {
        $this->recipeRepository = $recipeRepository;
    }

    public function handle(FetchRecipeByIdUseCase $fetchRecipeByIdUseCase): Recipe
    {
        $recipe = $this->recipeRepository->findById(
            $fetchRecipeByIdUseCase->getId()
        );

        return $recipe;
    }
}
