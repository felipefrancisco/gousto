<?php declare(strict_types=1);

namespace Domain\Exceptions;

/**
 * Class InvalidBulletpointTypeException
 * @package Domain\Exceptions
 */
class InvalidBulletpointTypeException extends \Exception
{
    protected $code = 2;

    /**
     * @param string $entry
     * @return InvalidBulletpointTypeException
     */
    public static function withEntry(string $entry): self
    {
        return new self(
            sprintf('Invalid `Bulletpoint` `%s`. Expected `string`.', $entry)
        );
    }
}
