<?php declare(strict_types=1);

namespace Domain;

use Domain\Exceptions\InvalidRateRangeException;

/**
 * Class Rating
 * @package Domain
 */
class Rating implements \JsonSerializable
{
    protected $rate;

    /**
     * Fat constructor.
     * @param int $rate
     * @throws InvalidRateRangeException
     */
    public function __construct(int $rate)
    {
        if ($rate < 1 || $rate > 5) {
            throw InvalidRateRangeException::withRate($rate);
        }

        $this->rate = $rate;
    }

    /**
     * @return int
     */
    public function rate(): int
    {
        return $this->rate;
    }

    public function jsonSerialize()
    {
        return $this->rate();
    }

    public function __toString()
    {
        return (string)($this->rate);
    }
}
