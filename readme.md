This project uses dockerized dependencies. Check the `docker-compose.yml` file for details.

## Getting Started
- Run `composer bootstrap` to set up the `.env` file.
- Run `docker-compose up` to start the containers.
- Wait for dependencies download via composer. It could take a few seconds.
- Import the `postman.json` export in the `docs/` folder into Postman.
- Use `localhost:8000` as the host for the requests.

Please remember to run `composer bootstrap` first, otherwise Postman requests will error.

## Tests
- There are `functional` tests for every `UseCase`.
- Run the test cases using `composer test`. A dockerized php container will be brought up to run the tests in isolation.

## Storage
- One of the tests constraints was to not use a database to handle the data. Although SQLite was suggested, I'm not very familiar with it, so I decided to let the `.csv` file to act as primary datasource for this application.

**Note:** Redis was also a choice, but I decided to go csv so I could explore more the persistence solution. 

## Framework Choice
- I decided to use Lumen for this project because it's very light and fast compared to full-fledged frameworks. I'm only using its basic bootstraping scripts, routes and requests - all the rest of the architecture follows best practices to ensure the solution remains as much framework-agnostic as possible.

Any questions, feel free to get in touch via **felipefrancisco@outlook.com** :)