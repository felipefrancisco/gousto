<?php declare(strict_types=1);

namespace Domain;

/**
 * Class Calories
 * @package Domain
 */
class Calories
{
    protected $amount;

    /**
     * Calories constructor.
     * @param int $amount
     */
    public function __construct(int $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function amount(): int
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function asKcal(): int
    {
        return ($this->amount / 1000);
    }
}
