<?php declare(strict_types=1);

namespace Domain;

use Domain\Exceptions\InvalidBulletpointsListSizeException;
use Domain\Exceptions\InvalidBulletpointTypeException;
use Illuminate\Support\Collection;

/**
 * Class BulletpointCollection
 * @package Domain
 */
class BulletpointCollection extends Collection
{
    public const MAX_SIZE = 3;

    /**
     * BulletpointCollection constructor.
     * @param array $items
     * @throws \Exception
     */
    /**
     * BulletpointCollection constructor.
     * @param array $items
     * @throws InvalidBulletpointTypeException
     * @throws InvalidBulletpointsListSizeException
     */
    public function __construct($items = [])
    {
        $size = count($items);

        $this->validateMaxSize($size);
        $this->validateType($items);

        parent::__construct($items);
    }

    /**
     * @return string
     */
    /**
     * @return string
     */
    public function second(): string
    {
        return $this->get(1);
    }

    /**
     * @return string
     */
    /**
     * @return string
     */
    public function third(): string
    {
        return $this->get(2);
    }

    /**
     * @param int $size
     * @throws InvalidBulletpointsListSizeException
     */
    private function validateMaxSize(int $size): void
    {
        if ($size > self::MAX_SIZE) {
            throw InvalidBulletpointsListSizeException::withSize($size);
        }
    }

    /**
     * @param array $items
     * @throws InvalidBulletpointTypeException
     */
    private function validateType(array $items): void
    {
        foreach ($items as $item) {
            if (is_null($item)) {
                continue;
            }

            if (!is_string($item)) {
                throw InvalidBulletpointTypeException::withEntry((string)($item));
            }
        }
    }
}
