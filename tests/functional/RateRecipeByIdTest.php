<?php

namespace Tests\Functional;

use Domain\Repositories\RecipeRepositoryInterface;
use Infrastructure\Repositories\Csv\CsvRecipeRepository;
use Test\TestCase;

class RateRecipeByIdTest extends TestCase
{
    /** @var RecipeRepositoryInterface */
    protected $recipeRepository;

    public function setUp()
    {
        parent::setUp();

        $this->recipeRepository = app(CsvRecipeRepository::class);
    }

    /** @test */
    public function itSucceedsToRateExistingRecipe(): void
    {
        $rating = 5;

        $data = [
            'rating' => $rating
        ];

        $this->json('POST', '/recipes/1/rate', $data);

        $this->seeStatusCode(200);

        $recipe = $this->recipeRepository->findById(1);
        $this->assertEquals($rating, $recipe->rating()->rate());
    }

    /** @test */
    public function itFailsToRateNonExistingRecipe(): void
    {
        $this->post("recipes/1000/rate", [
            'rating' => 5
        ]);

        $this->seeStatusCode(404);

        $this->seeJson([
            'messages' => [
                'Recipe `1000` was not found'
            ]
        ]);
    }

    /** @test */
    public function itFailsToRateAboveFive(): void
    {
        $this->post("recipes/1/rate", [
            'rating' => 6
        ]);

        $this->seeStatusCode(400);

        $this->seeJson([
            'messages' => [
                'Invalid Rate `6`. Expected between 1 and 5.'
            ]
        ]);
    }

    /** @test */
    public function itFailsToRateLowerThanOne(): void
    {
        $this->post("recipes/1/rate", [
            'rating' => 0
        ]);

        $this->seeStatusCode(400);

        $this->seeJson([
            'messages' => [
                'Invalid Rate `0`. Expected between 1 and 5.'
            ]
        ]);
    }
}
