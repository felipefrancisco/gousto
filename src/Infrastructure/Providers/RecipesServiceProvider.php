<?php declare(strict_types=1);

namespace Infrastructure\Providers;

use Application\Services\FetchRecipeByIdService;
use Application\Services\FetchRecipeByIdUseCase;
use Illuminate\Support\ServiceProvider;
use Infrastructure\Factories\RecipeFactory;
use Infrastructure\Repositories\Csv\CsvRecipeRepository;
use League\Csv\Reader;
use League\Csv\Writer;

/**
 * Class RecipesServiceProvider
 * @package Infrastructure\Providers
 */
class RecipesServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CsvRecipeRepository::class, function() {

            $path = env('RECIPE_CSV_PATH');

            // Loads the .csv from rroject's root + path.
            $recipeCsvPath = sprintf('%s/../../%s', dirname(__DIR__), $path);

            $reader = Reader::createFromPath($recipeCsvPath, 'r');
            $reader->setHeaderOffset(0);

            $writer = Writer::createFromPath($recipeCsvPath);

            $RecipeFactory = app(RecipeFactory::class);

            return new CsvRecipeRepository(
                $reader,
                $writer,
                $RecipeFactory
            );
        });

        $this->app->singleton(RecipeFactory::class, function() {
            return new RecipeFactory();
        });

        $this->app->singleton(RecipeTransformer::class);
    }
}
