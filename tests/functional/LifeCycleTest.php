<?php

namespace Tests\Functional;

use Application\Services\RateRecipeByIdService;
use Domain\Repositories\RecipeRepositoryInterface;
use Illuminate\Http\Request;
use Infrastructure\Http\Requests\CreateRecipeRequest;
use Infrastructure\Http\Requests\FetchRecipeByIdRequest;
use Infrastructure\Http\Requests\RateRecipeByIdRequest;
use Infrastructure\Http\Requests\UpdateRecipeByIdRequest;
use Infrastructure\Repositories\Csv\CsvRecipeRepository;
use Test\TestCase;

class LifeCycleTest extends TestCase
{
    /** @var RecipeRepositoryInterface */
    protected $recipeRepository;

    public function setUp()
    {
        parent::setUp();

        $this->recipeRepository = app(CsvRecipeRepository::class);
    }

    /** @test */
    public function itSurvivesLifecycle(): void
    {
        $data = $this->validCreateRecipePayload();

        $response = $this->json('POST', '/recipes', $data)->response->getContent();
        $this->seeStatusCode(201);

        $response = json_decode($response);
        $id = $response->data->id;

        $recipe = $this->recipeRepository->findById($id);
        $this->assertEquals($id, $recipe->id());

        $data = $this->validRatePayload();

        $this->json('POST', sprintf('/recipes/%d/rate', $id), $data);
        $this->seeStatusCode(200);

        $recipe = $this->recipeRepository->findById($id);
        $this->assertEquals($data['rating'], $recipe->rating()->rate());

        $data = $this->validUpdateRecipePayload();

        $this->json('PUT', sprintf('/recipes/%d', $id), $data);
        $this->seeStatusCode(200);

        $recipe = $this->recipeRepository->findById($id);
        $this->assertEquals($data['title'], $recipe->title());
        $this->assertEquals($data['slug'], $recipe->slug());

        $this->json('GET', sprintf('/recipes/%d', $id));
        $this->seeStatusCode(200);
    }

    private function validCreateRecipePayload(): array
    {
        return [
            "box_type" => "vegetarian",
            "title" => "Marmite",
            "slug" => "marmite",
            "short_title" => "",
            "marketing_description" => "Marmite",
            "calories_kcal" => 9999,
            "protein_grams" => 9999,
            "fat_grams" => 9999,
            "carbs_grams" => 999,
            "bulletpoint1" => "Bullet 1",
            "bulletpoint2" => "Bullet 2",
            "bulletpoint3" => "Bullet 3",
            "recipe_diet_type_id" => "fish",
            "season" => "season",
            "base" => "base",
            "protein_source" => "beef",
            "preparation_time_minutes" => 9999,
            "shelf_life_days" => 9999,
            "equipment_needed" => "equipment",
            "origin_country" => "Italy",
            "recipe_cuisine" => "asian",
            "in_your_box" => "one, two, three things",
            "gousto_reference" => 9999
        ];
    }

    private function validRatePayload(): array
    {
        return [
            'rating' => 5
        ];
    }

    private function validUpdateRecipePayload(): array
    {
        return [
            "box_type" => "vegetarian",
            "title" => "I HAVE CHANGED",
            "slug" => "i-have-changed",
            "short_title" => "",
            "marketing_description" => "Marmite",
            "calories_kcal" => 9999,
            "protein_grams" => 9999,
            "fat_grams" => 9999,
            "carbs_grams" => 999,
            "bulletpoint1" => "Bullet 1",
            "bulletpoint2" => "Bullet 2",
            "bulletpoint3" => "Bullet 3",
            "recipe_diet_type_id" => "fish",
            "season" => "season",
            "base" => "base",
            "protein_source" => "beef",
            "preparation_time_minutes" => 9999,
            "shelf_life_days" => 9999,
            "equipment_needed" => "equipment",
            "origin_country" => "Italy",
            "recipe_cuisine" => "asian",
            "in_your_box" => "one, two, three things",
            "gousto_reference" => 9999
        ];
    }
}
