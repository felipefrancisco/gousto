<?php

namespace Infrastructure\Http\Exceptions;

use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\Response;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];
    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }
    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        if (env('APP_DEBUG')) {
            return parent::render($request, $e);
        }

        $status = Response::HTTP_BAD_REQUEST;
        $message = $e->getMessage();

        switch (true) {

            case ($e instanceof HttpResponseException):
                $status = Response::HTTP_INTERNAL_SERVER_ERROR;
                break;

            case ($e instanceof MethodNotAllowedHttpException):
                $status = Response::HTTP_METHOD_NOT_ALLOWED;
                break;

            case ($e instanceof NotFoundHttpException):
                $status = Response::HTTP_NOT_FOUND;
                break;

            case ($e instanceof AuthorizationException):
                $status = Response::HTTP_FORBIDDEN;
                break;

            case ($e instanceof \Dotenv\Exception\ValidationException && $e->getResponse()):
                $status = Response::HTTP_BAD_REQUEST;
                break;

            case ($e instanceof ValidationException):
                $status = Response::HTTP_UNPROCESSABLE_ENTITY;
                $message = $e->validator->getMessageBag()->all();
                break;

            case ($e instanceof \TypeError):
                $status = Response::HTTP_INTERNAL_SERVER_ERROR;
                break;
        }

        if (!is_array($message)) {
            $message = [ $message ];
        }

        return response()->json([
            'messages' => $message
        ], $status);
    }
}
