<?php declare(strict_types=1);

namespace Domain\Exceptions;

/**
 * Class InvalidBulletpointsListSizeException
 * @package Domain\Exceptions
 */
class InvalidBulletpointsListSizeException extends \Exception
{
    protected $code = 1;

    /**
     * @param int $size
     * @return InvalidBulletpointsListSizeException
     */
    public static function withSize(int $size): self
    {
        return new self(
            sprintf('Invalid Bulletpoints List size of `%d`. Expected equal or less than 3.', $size)
        );
    }
}
