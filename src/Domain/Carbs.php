<?php declare(strict_types=1);

namespace Domain;

/**
 * Class Carbs
 * @package Domain
 */
class Carbs
{
    protected $amount;

    /**
     * Carbs constructor.
     * @param int $amount
     */
    public function __construct(int $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function amount(): int
    {
        return $this->amount;
    }

    /**
     * @return int
     */
    public function asGrams(): int
    {
        return $this->amount;
    }
}
