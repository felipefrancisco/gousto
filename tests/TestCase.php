<?php

namespace Test;

abstract class TestCase extends \Laravel\Lumen\Testing\TestCase
{
    public function setUp()
    {
        $this->resetDatabase();

        parent::setUp();
    }

    /**
     * Creates the application.
     *
     * @return \Laravel\Lumen\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../src/Infrastructure/Configuration/Bootstrap/app.php';

        return $app;
    }

    private function resetDatabase(): void
    {
        copy(__DIR__ . '/fixtures/initial.csv', __DIR__ . '/fixtures/recipes.csv');
    }
}
