<?php declare(strict_types=1);

namespace Infrastructure\Http\Requests;

use Illuminate\Http\Request;

class FetchAllRecipesByCuisineRequest extends Request
{
    public function rules()
    {
        return [
            "cuisine" => ["required", "string"],
            'page' => ['required', 'integer', 'min:1']
        ];
    }

    public function getCuisine(): string
    {
        $cuisine = collect($this->all())->get('cuisine');

        return (string)($cuisine);
    }

    public function getPage(): int
    {
        return (int)$this->get('page');
    }

    public function getLimit(): int
    {
        return 10;
    }
}